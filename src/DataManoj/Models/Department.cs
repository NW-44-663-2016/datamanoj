﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataManoj.Models
{
    public class Department
    {
        [ScaffoldColumn(false)]
        [Key]
        public int DeptID { get; set; }
        [Required]
        [Display(Name = "Dept Name")]
        public string DeptName { get; set; }
        [Display(Name = "Dept Address")]
        public string DeptAddress { get; set; }
        public string contact { get; set; }
        [ScaffoldColumn(false)]
        public int LocationId { get; set; }
      //  public virtual Location Location { get; set; }
    }
}
