﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataManoj.Models
{
    public class Location
    {
        [ScaffoldColumn(false)]
        public int LocationID { get; set; }
        [Display(Name = "Country")]
        public string Country { get; set; }
        [Display(Name = "Place")]
        public string Place { get; set; }
        public string State { get; set; }
        public string StateAbbreviation { get; set; }
        public string County { get; set; }
        public string ZipCode { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
