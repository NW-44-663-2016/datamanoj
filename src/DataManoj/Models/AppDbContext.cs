﻿using Microsoft.Data.Entity;

namespace DataManoj.Models
{
    public class AppDbContext:DbContext
    {
        public DbSet<Location> Locations { get; set; }
        public DbSet<Department> Departments { get; set; }
    }
}
